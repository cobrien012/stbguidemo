from kivy.config import Config
# Config.set('graphics', 'show_cursor', 0)
Config.set('graphics', 'fullscreen', 'fake')
Config.set('graphics', 'height', 480)
Config.set('graphics', 'width', 800)

from functools import partial
from kivy.app import App
from kivy.core.window import Window
from kivy.uix.accordion import Accordion, AccordionItem
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.togglebutton import ToggleButton
import time
import random
import pdb
import STBmain


class StbGui(App):
	def build(self):
		parent = Accordion()

		mainSlide = AccordionItem(title='Main')
		mainBox = BoxLayout(orientation='vertical')
		Title = Label(text = 'UPenn Haptics Smart Task Board', font_size= '40sp')
		Title.size_hint = (1, .5)
		
		scoreBox = BoxLayout(orientation='horizontal')
		scores = []
		for i in range(0,5):
			scores.append(Label(text='1', font_size= '40sp'))
			scoreBox.add_widget(scores[i])

		startBtn = ToggleButton(text='Start',font_size= '40sp')
		startBtn.bind(on_release=partial(self.testStart, scores))
		startBtn.bind(on_press=self.startNote)
		self.task = '1'
		self.currentUser = 'Default'
		statusLabel = Label(text = 'Subject: ' + self.currentUser + '\n Task: ' + str(self.task),font_size= '24sp')
		statusLabel.size_hint = (1,0.5)
		statusLabel.border = (20, 20, 40, 40)

		mainBox.add_widget(Title)
		mainBox.add_widget(startBtn)
		mainBox.add_widget(statusLabel)
		mainBox.add_widget(scoreBox)
		mainSlide.add_widget(mainBox)


		userSlide = AccordionItem(title='User Settings', multiline=False)
		userBox = BoxLayout(orientation='vertical')
		
		boxSize = 0.25
		userLabel = Label(text='Subject Name: ' + self.currentUser, font_size= '24sp')
		userLabel.size_hint = (1,boxSize)

		userName = TextInput(multiline=False, font_size= '24sp',padding=[20, 15] )
		userName.size_hint = (1,boxSize)
		userName.bind(on_text_validate=partial(self.updateUserName, userLabel, statusLabel))

		userName.size_hint = (1,boxSize)
		userBlank = Label()

		userBox.add_widget(userLabel)
		userBox.add_widget(userName)
		userBox.add_widget(userBlank)
		userSlide.add_widget(userBox)

		taskSlide = AccordionItem(title='Task Selection')
		taskBox = BoxLayout(orientation='vertical')
		taskSize = '36sp'
		taskBtns = []
		taskBtns.append(ToggleButton(text= 'Task 1', group='tasks',font_size= taskSize,state='down'))
		taskBtns[0].bind(on_press=partial(self.setTask, 1, statusLabel))
		taskBtns.append(ToggleButton(text= 'Task 2', group='tasks',font_size= taskSize))
		taskBtns[1].bind(on_press=partial(self.setTask, 2, statusLabel))
		taskBtns.append(ToggleButton(text= 'Task 3', group='tasks',font_size= taskSize))
		taskBtns[2].bind(on_press=partial(self.setTask, 3, statusLabel))

		for btn in taskBtns:
			taskBox.add_widget(btn)
		
		taskSlide.add_widget(taskBox)
		
		self.graphing = 0
		self.debugStatus = ''
		debugSlide = AccordionItem(title='Debug & Graphing')
		debugBox = BoxLayout()
		debugDropDown = DropDown()
		graphBtns = []
		graphBtns.append(Button(text='None'))
		graphBtns.append(Button(text='Force and Torque'))
		graphBtns.append(Button(text='Mini40 Channel Voltages'))
		graphBtns.append(Button(text='Accelerometers'))

		for btn in graphBtns:
			btn.size_hint_y = None
			btn.height = 60
			btn.bind(on_release=lambda btn: debugDropDown.select(btn.text))
			debugDropDown.add_widget(btn)

		debugBtn = Button(text='Debug Graphing', size_hint=(1, None))
		debugBtn.bind(on_release=debugDropDown.open)
		debugDropDown.bind(on_select=partial(self.debugUpdate, statusLabel))

		debugBox.add_widget(debugBtn)
		debugSlide.add_widget(debugBox)

		parent.add_widget(mainSlide)
		parent.add_widget(userSlide)
		parent.add_widget(taskSlide)
		parent.add_widget(debugSlide)
		mainSlide.collapse = False

		return parent

	def testStart(self, scores, obj):
		print 'START... Subject Name:' + self.currentUser + " Task: " + str(self.task) + self.debugStatus
		STBmain.RunSTB(self.currentUser, self.task,self.debugStatus)
		obj.text = 'Start'
		obj.state = 'normal'

		for score in scores:
			score.text = str(random.randint(1,5))

	def debugUpdate(self, statusLabel, obj, text):
		if text != 'None':
			self.debugStatus = 'DEBUG: ' + text

		else:
			self.debugStatus = ''

		print self.debugStatus
		obj.text = text	
		self.updateStatus(statusLabel)	

	def updateStatus(self, statusLabel):
		statusLabel.text = 'Subject: ' + self.currentUser + '\n Task: ' + str(self.task) + '\n' + self.debugStatus

	def startNote(self, obj):
		obj.text = 'Running ...'

	def setTask(self, taskNum, statusLabel, obj):
		print'TASK' + str(taskNum)
		self.task = taskNum
		self.updateStatus(statusLabel)	

	def updateUserName(self, userLabel, statusLabel, obj):
		print obj.text
		userLabel.text = 'Subject Name: ' + obj.text
		self.currentUser = obj.text
		obj.text = ''
		self.updateStatus(statusLabel)	


if __name__ == '__main__':
	StbGui().run()
