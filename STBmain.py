import numpy as np
import pylab as pl
import glob 
import os
import pdb
import serial
import subprocess
import sys
import threading
import time
import cv
from cv2 import VideoCapture, VideoWriter
from cv2.cv import CV_FOURCC
from STBcython import *

# Error for HapticsSTB class
class EmptyPacketError(Exception):
	pass

# Class for the Sensor STB. Currently manages initialization, stop, start and reading raw serial packets
class STBSensor:

	def __init__(self, serial_device, sample_rate):
		self.device = serial_device
		if sample_rate > int('0xFFFF',16):
			raise ValueError
		high = (sample_rate&int('0xFF00',16))>>8
		low = sample_rate&int('0x00FF',16)
		self.sample_rate = chr(high)+chr(low)

	def start(self):
		self.device.write('\x01' + self.sample_rate)
		self.packet_old = 300

	def stop(self):
		self.device.write('\x02')
		self.device.flush()

	def read(self):
		dat = self.device.read(31)

		if dat == '' or len(dat) != 31:
			raise EmptyPacketError
		self.packet = ord(dat[30])
		
		if self.packet_old == 300:
			self.packet_old = self.packet
		elif self.packet != (self.packet_old+1)%256:
			print 'MISSED PACKET', self.packet, self.packet_old
			self.packet_old = self.packet
		else:
			self.packet_old = self.packet
		
		return dat

	def readMini40(self):
		dat = self.readSerial()
		return Serial2M40(dat)

	def readACC(self):
		dat = self.readSerial()
		return Serial2Acc(dat)

	def close(self):
		self.stop()
		self.device.close()

def ArgParse(args):
	# Dict for command line inputs, contains default values
	inputs = {	'subject': 2,
				'task': 1,
				'graphing': 0,
				'line_length': 250,
				'bias_sample': 100,
				'update_interval': 50,
				'sample_time': 5,
				'write_data': 1,
				'sample_rate': 3000,
				'pedal': 1,
				'video_capture': 1,
				'compress': 0,
	}

	# Message displayed for command line help
	help_message = """ ******
	-subject: Subject ID number (2)
	-task: Task ID number (1)
	-pedal: use foot pedal for controlling sampling (1)
	-video_capture: Record video from Da Vinci (1)
	-write_data: Write data to timestamped file (1)
	-compress: Compresses data after recording, produces a 7z archive (0)
	-bias_sample: Number of samples averaged to get Mini40 biasing vector (100)
	-sample_time: Length of sample in seconds (5)
	-sample_rate: data sampling rate in Hz (default 3kHz, forced to 500Hz for plotting)
	-graphing: reduce sample rate and display line plots for debugging
	    1: F/T graph
	    2: Mini40 Channel Voltages
	    3: Accelerometer Voltages
	    4: Single Point Position
	(GRAPHING OPTIONS)
	-line_length: number of samples shown on graph (250)
	-update_interval: Number of samples between plot updates (50)
	*****
	"""

	## Input handling, inputs beginning with '-' are considered commands, following
	# string is converted to an int and assigned to the inputs dict
	try:
		if len(args) > 1:
			for arg in range(0,len(args)):
				command = args[arg]
				if command[0] == '-':
					if command == '-help' or command == '-h':
						print help_message
						sys.exit()
					else:
						inputs[command[1:].lower()] = int(args[arg+1])
		return inputs
	except (NameError, ValueError, IndexError, KeyError) as e:
		print "Invalid Command!: " + str(e)
		sys.exit()

def DeviceID(sample_rate):

	devices = glob.glob('/dev/ttyACM*')

	if len(devices) < 2:
		print 'NOT ENOUGH DEVICES CONNECTED, EXITING...'
		return

	STB = PedalSerial = 0

	for dev in devices:
		# Step through devices, pinging each one for a device ID
		test_device = serial.Serial(dev, timeout=0.1)
		test_device.write('\x02')
		time.sleep(0.05)
		test_device.flushInput()
		test_device.write('\x03')
	 	
		devID = test_device.read(200)[-1]	#Read out everything in the buffer, and look at the last byte for the ID

		if devID == '\x01':
			test_device.timeout = 0.05
			STB = STBSensor(test_device, sample_rate)
		elif devID == '\x02':
			PedalSerial = test_device
			PedalSerial.timeout = 0
		else:
			print 'UNKNOWN DEVICE, EXITING...'
			return

	if not STB or not PedalSerial:
		return ()
	else:
		return (STB, PedalSerial)

def GraphingSetup(graphing, line_length):

	pl.ion()

	if graphing in [1,2,3]:
		start_time = -1*(line_length-1)/500.0
		times = np.linspace(start_time, 0, line_length)

	# Force/Torque Graphing
	if graphing == 1:

		f, (axF, axT) =pl.subplots(2,1, sharex=True)

		axF.axis([start_time, 0,-5,5])
		axF.grid()
		axT.axis([start_time, 0,-.5,.5])
		axT.grid()

		FXline, = axF.plot(times, [0] * line_length, color = 'r')
		FYline, = axF.plot(times, [0] * line_length, color = 'g')
		FZline, = axF.plot(times, [0] * line_length, color = 'b')
		TXline, = axT.plot(times, [0] * line_length, color = 'c')
		TYline, = axT.plot(times, [0] * line_length, color = 'm')
		TZline, = axT.plot(times, [0] * line_length, color = 'y')

		axF.legend([FXline, FYline, FZline], ['FX', 'FY', 'FZ'])
		axT.legend([TXline, TYline, TZline], ['TX', 'TY', 'TZ'])

		plot_objects = (FXline, FYline, FZline, TXline, TYline, TZline)

		pl.draw()

	# Mini40 Voltage Graphing
	elif graphing == 2:

		pl.axis([start_time, 0,-2,2])
		pl.grid()

		C0line, = pl.plot(times, [0] * line_length, color = 'brown')
		C1line, = pl.plot(times, [0] * line_length, color = 'yellow')
		C2line, = pl.plot(times, [0] * line_length, color = 'green')
		C3line, = pl.plot(times, [0] * line_length, color = 'blue')
		C4line, = pl.plot(times, [0] * line_length, color = 'purple')
		C5line, = pl.plot(times, [0] * line_length, color = 'gray')

		pl.legend([C0line, C1line, C2line, C3line, C4line, C5line], 
			['Channel 0', 'Channel 1','Channel 2','Channel 3','Channel 4','Channel 5'], loc=2)

		plot_objects = (C0line, C1line, C2line, C3line, C4line, C5line)
		pl.draw()

	#Accelerometer Voltage Graphing
	elif graphing == 3:

		f, (ax1, ax2, ax3) =pl.subplots(3,1, sharex=True)

		ax1.axis([start_time, 0,-.1,3.4])
		ax2.axis([start_time, 0,-.1,3.4])
		ax3.axis([start_time, 0,-.1,3.4])
		ax1.grid()
		ax2.grid()
		ax3.grid()

		A1Xline, = ax1.plot(times, [0] * line_length, color = 'r')
		A1Yline, = ax1.plot(times, [0] * line_length, color = 'g')
		A1Zline, = ax1.plot(times, [0] * line_length, color = 'b')
		A2Xline, = ax2.plot(times, [0] * line_length, color = 'r')
		A2Yline, = ax2.plot(times, [0] * line_length, color = 'g')
		A2Zline, = ax2.plot(times, [0] * line_length, color = 'b')
		A3Xline, = ax3.plot(times, [0] * line_length, color = 'r')
		A3Yline, = ax3.plot(times, [0] * line_length, color = 'g')
		A3Zline, = ax3.plot(times, [0] * line_length, color = 'b')

		plot_objects = (A1Xline, A1Yline, A1Zline, A2Xline, A2Yline, A2Zline, A3Xline, A3Yline, A3Zline)
		pl.draw()

	# 2D Position Plotting
	elif graphing == 4:

		pl.axis([-.075, .075, -.075, .075])
		pl.grid()
		touch_point, = pl.plot(0,0, marker="o", markersize=50)

		plot_objects = (touch_point,)
		pl.draw()

	else:
		print "INVALID GRAPHING MODE"
		return 0

	return plot_objects

class OpenCVThread(threading.Thread):
	def __init__(self, cap, out):
		threading.Thread.__init__(self)
		self.stop = threading.Event()
		self.out = out
		self.cap = cap
		
	def run(self):
		while not self.stop.is_set():
			ret, frame = self.cap.read()
			if ret == True:
				self.out.write(frame)

def RunSTB(subject, task, graphing_input):

	#err = subprocess.call(['v4l2-ctl', '-i 4'])
	
	#if err == 1:
	#	print "VIDEO CAPTURE ERROR, CHECK CARD AND TRY AGAIN"
	#	return

	cap = VideoCapture(-1)
	fourcc = CV_FOURCC(*'XVID')

	g_input_dict = {'none':0, 'Force and Torque':1, 'Mini40 Channel Voltages':2,'Accelerometers':3}
	graphing = g_input_dict[graphing_input]
	line_length = 1000

	if graphing:
		sample_rate = 500
		plot_objects = GraphingSetup(graphing, line_length)
	else:
		sample_rate = 3000

	(STB, PedalSerial) = DeviceID(sample_rate)

	bias_length = 300
	bias_hist = np.zeros((6,bias_length))
	STB.start()

	for ii in range(0, bias_length):

		try:
			dat = STB.read()
		except EmptyPacketError:
			print 'NOTHING RECIEVED, EXITING...'
			STB.close()
			PedalSerial.close()

		bias_hist[:,ii] = Serial2M40Volts(dat)

	bias = np.mean(bias_hist, axis=1).T
	STB.stop()

	num_samples = sample_rate*6400

	try:
		# Pedal input blocking, single or double tap starts trial, triple quits
		print 'WAITING FOR PEDAL INPUT...'
		pedal_input = ''
		while pedal_input != '\x01':
			pedal_input = PedalSerial.read()

		# File operations, checks to make sure folders exist and  creates timestamp
		data_dir = 'TestData'
		subject_dir = 'Sbj'+subject
		test_filename =  'S' + subject + 'T' + str(task) +'_' + time.strftime('%m-%d_%H-%M-%S')
		test_path = data_dir + '/' + subject_dir + '/' + test_filename

		if [] == glob.glob(data_dir):
			print "MAKING " + data_dir
			os.mkdir(data_dir)

		if [] == glob.glob(data_dir + '/' + subject_dir):
			print "MAKING " + subject_dir
			os.mkdir(data_dir + '/' + subject_dir)

		# Video prep
		out = VideoWriter(test_path+'.avi',fourcc, 29.970,(int(cap.get(cv.CV_CAP_PROP_FRAME_WIDTH)),int(cap.get(cv.CV_CAP_PROP_FRAME_HEIGHT))))
		videoThread = OpenCVThread(cap, out)
		videoThread.start()

		print 'STARTING DATA COLLECTION...'
		start = time.time()

		DAT_hist = np.zeros((num_samples, 15))

		STB.start()

		for ii in range(0,num_samples):

			try:
				dat = STB.read()
			except EmptyPacketError:
				print 'NOTHING RECEIVED, EXITING...'
				STB.stop()
				PedalSerial.close()
				videoThread.stop.set()
				return

			DAT_hist[ii, 0:15] = Serial2Data(dat, bias)
			
			pedal_input = PedalSerial.read()

			if graphing and ii % 100 == 0 and ii > line_length:
					updated_data = DAT_hist[(ii + 1 - line_length):(ii+1),:]
					GraphingUpdater(graphing, updated_data, plot_objects)


			if pedal_input == '\x03' or pedal_input == '\x02':
				print 'QUITTING...'
				print 'PEDAL STOP'
				break

		STB.stop()
		videoThread.stop.set()

		print 'FINISHED SAMPLING'
		print time.time()-start

		print 'WRITING DATA TO %s...' %test_filename

		np.savetxt(test_path + '.csv', DAT_hist[:(ii+1),0:15], delimiter=",")

		print 'FINISHED WRITING'

		print '*'*80

	except KeyboardInterrupt:
		print '***** ENDING TESTING *****'
		# STBserial.close()
		STB.stop()
		PedalSerial.close()
		videoThread.stop.set()
